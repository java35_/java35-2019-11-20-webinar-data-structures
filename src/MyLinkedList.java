import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyLinkedList implements List<Integer>{
	
	private Node root;
	private Node tail;
	
	class Node {
		Node previousNode;
		Node nextNode;
		Integer value;
		
		Node(Integer value) {
			this.value = value;
		}
	}
	
	private int size;
	
	public MyLinkedList() {
		root = null;
		size = 0;
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) { //8
//		if (root == null)
//			return false;
//		Node currentNode = root;
//		int value = (Integer)o;
//		
//		do {
//			if (currentNode.value == value)
//				return true;
//			currentNode = currentNode.nextNode;
//		} while(currentNode != null);
		
		Node currentNode = root;
		int value = (Integer)o;
		
		while(currentNode != null) {
			if (currentNode.value == value)
				return true;
			currentNode = currentNode.nextNode;
		}
		
		return false;
	}

	@Override
	public Iterator<Integer> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Integer e) {
		Node newNode = new Node(6);
		if (root == null) {
			root = newNode;
			size++;
			return true;
		}
		
		newNode.nextNode = root;
		root.previousNode = newNode;
		root = newNode;
		size++;
		
		return true;
	}

	@Override
	public boolean remove(Object o) { //10
		// size == 0
		if (isEmpty())
			return false;
		
		int value = (Integer)o;
		
		// size == 1
		if (size == 1) {
			if (root.value == value) {
				root = null;
				size--;
				return true;
			}
			return false;
		}
		
		// size > 1
		Node currentNode = root.nextNode;
		
		while(currentNode != null) {
//			if (currentNode.value == value) {
//				Node previousNode = currentNode.previousNode;
//				Node nextNode = currentNode.nextNode;
//				
//				previousNode.nextNode = currentNode.nextNode;
//				if (nextNode != null) {
//					nextNode.previousNode = currentNode.previousNode;
//				}
//				size--;
//				return true;
//			}
//			currentNode = currentNode.nextNode;
			
			if (currentNode.nextNode != null)
				currentNode.nextNode.previousNode = currentNode.previousNode;
			if (currentNode.previousNode != null)
				currentNode.previousNode.nextNode = currentNode.nextNode;
		}
		
		
		
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends Integer> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends Integer> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer set(int index, Integer element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(int index, Integer element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<Integer> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<Integer> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}